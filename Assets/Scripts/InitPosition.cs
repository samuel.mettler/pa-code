using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitPosition : MonoBehaviour
{
    public GameObject toMove;
    public GameObject pointer;
    //public GameObject buttons; 
    public GameObject pen;

    private bool isInit;     
    private bool firstIsInit;

    private Vector3 firstClick;
    private Vector3 secondClick;

    // Update is called once per frame
    void Start(){

        isInit = false;
    }
    void Update()
    {
        if (!isInit)
        {
            if(Input.GetKeyDown(KeyCode.Space)){
                if(!firstIsInit)
                {
                    firstClick = pointer.transform.position;
                    firstIsInit = true;
                    Debug.Log("First Click");
                    Debug.Log(firstClick);
                }
                else{
                    secondClick = pointer.transform.position;   
                    isInit = true;
                    Debug.Log("Second Click");

                    UpdateTransformForScale();
                }
            }
        }

        else{
            if (Input.GetKeyDown("f")){
                isInit = false;
                firstIsInit = false;
                Debug.Log("reset position of support possible");
                toMove.transform.eulerAngles = new Vector3(0,0,0);
            }
        }
    }

    void UpdateTransformForScale()
    {

        float distanceX = Mathf.Abs(firstClick.x - secondClick.x);
        float distanceY = Mathf.Abs(firstClick.y - secondClick.y);
        float distanceZ = Mathf.Abs(firstClick.z - secondClick.z);

        float notUsed = Mathf.Min(distanceX, distanceY, distanceZ);
        float newX, newY, newZ;
        if (notUsed == distanceX){
            newX = 0.01f;
            newY = distanceY;
            newZ = distanceZ;
        }
        else if (notUsed == distanceY){
            newX = distanceX;
            newY = 0.01f;
            newZ = distanceZ;
        }
        else{
            newX = distanceX;
            newY = distanceY;
            newZ = 0.01f;
        }

        
        toMove.transform.localScale = new Vector3(newX, newY, newZ);
        
        Vector3 middlePoint = (firstClick + secondClick) / 2f;
        toMove.transform.position = middlePoint;
        if (Mathf.Abs(pen.transform.eulerAngles.x) < 45 && Mathf.Abs(pen.transform.eulerAngles.y) < 45  && Mathf.Abs(pen.transform.eulerAngles.z) < 45 ){
            
            if(Mathf.Abs(pen.transform.eulerAngles.x) > 0 && Mathf.Abs(pen.transform.eulerAngles.y) < 15 && Mathf.Abs(pen.transform.eulerAngles.z) < 15)
                toMove.transform.eulerAngles = new Vector3(pen.transform.eulerAngles.x,0,0);
            else if (Mathf.Abs(pen.transform.eulerAngles.x) < 15 && Mathf.Abs(pen.transform.eulerAngles.y) > 0 && Mathf.Abs(pen.transform.eulerAngles.z)< 15)
                toMove.transform.eulerAngles = new Vector3(0, pen.transform.eulerAngles.y, 0);
            else
                toMove.transform.eulerAngles = new Vector3(0, 0, pen.transform.eulerAngles.z);
            }
        else
            toMove.transform.rotation = new Quaternion(0,0,0,1);
    }
}
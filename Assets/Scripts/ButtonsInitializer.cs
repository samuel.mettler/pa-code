using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsInitializer : MonoBehaviour
{

    private List<GameObject> buttonList;
    public GameObject button;
    private Vector3 offsetRow;
    private Vector3 offsetLine;
    private int maxButtonsRows;
    private int maxButtonsCols;
    private int nbMaxButtons;
    private float longueur; 
    private float largeur;
    private int lenList;
    public GameObject pointer;
    public GameObject parent;
    
    // Start is called before the first frame update
    
    void Start()
    {
        maxButtonsCols = 2;
        maxButtonsRows = 3;
        nbMaxButtons = maxButtonsCols * maxButtonsRows;
        buttonList = new List<GameObject>();
        
    }

    // Update is called once per frame
    void Update()
    {
        lenList = buttonList.Count;
        if(Input.GetKeyDown("b"))
        {   
            SpawnButton();
        }
        if(Input.GetKeyDown("l"))
        {
            LockButton();
        }
        if(Input.GetKeyDown("u"))
        {
            UnlockButton();
        }
        if(Input.GetKeyDown("n"))
        {
            ActivateButton();
        }        
        if(Input.GetKeyDown("m"))
        {
            DeleteButton();
        }
        if(Input.GetKeyDown("v"))
        {
            SpawnButtonOnPos();
        }
    }
 
    void SpawnButton(){ 
        longueur = Mathf.Max(this.transform.localScale.x, this.transform.localScale.z); 
        largeur = Mathf.Min(this.transform.localScale.x, this.transform.localScale.z);

        if(lenList == nbMaxButtons){
            Debug.Log("Can't spawn more");
            return;
        }
        var radians = 2 * Mathf.PI / nbMaxButtons * lenList;
        var vertical = Mathf.Sin(radians);
        var horizontal = Mathf.Cos(radians);
        var spawnDir = new Vector3(horizontal, 0, vertical);
        var spawnPos = this.transform.position + spawnDir * largeur / 3;
        buttonList.Add(Instantiate(button, spawnPos, this.transform.rotation, parent.transform));
    }

    void LockButton(){
        int lenList = buttonList.Count;
        Debug.Log("Locked");
        for(int i = 0; i < lenList; ++i){
            Rigidbody rigidbody; 

            rigidbody = buttonList[i].gameObject.GetComponent<Rigidbody>();
            rigidbody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
            
        }
    }

    void UnlockButton(){
        int lenList = buttonList.Count;
        Debug.Log("Unlocked");
        for(int i = 0; i < lenList; ++i){
             Rigidbody rigidbody; 

            rigidbody = buttonList[i].gameObject.GetComponent<Rigidbody>();
            rigidbody.constraints = RigidbodyConstraints.None;
        }
    }
    void ActivateButton(){
        Debug.Log("Active");
        for(int i = 0; i < lenList; ++i){
            buttonList[i].gameObject.GetComponent<PhysicsButton>().isActive = true;
            var cubeRenderer = buttonList[i].gameObject.GetComponentInChildren(typeof(MeshRenderer), true) as MeshRenderer;
            if (cubeRenderer != null)
                cubeRenderer.material.SetColor("_Color", Color.green);
        }
    }

    void DeleteButton(){
        if (lenList > 0){
            Debug.Log("Remove last");
            Debug.Log(lenList);

            Destroy(buttonList[lenList - 1].gameObject);
            buttonList.RemoveAt(buttonList.Count - 1);
            Debug.Log("Removed last");
            Debug.Log(buttonList.Count);
        }
        else{
            Debug.Log("Nothing to destroy");
        }

    }

    void SpawnButtonOnPos(){
        buttonList.Add(Instantiate(button, pointer.transform.position, pointer.transform.rotation, parent.transform));
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class PhysicsButton : MonoBehaviour
{
    [SerializeField] private float threshold = .01f;
    [SerializeField] private float deadZone = 1f;

    private bool isPressed;
    private Vector3 startPos;
    private ConfigurableJoint joint;
    public bool isActive;


    public UnityEvent onPressed, onReleased;
    // Start is called before the first frame update
    void Start()
    {
        isPressed = false;
        startPos = transform.localPosition;
        joint = GetComponent<ConfigurableJoint>();

    }

    // Update is called once per frame
    void Update()
    {
        if(isActive && !isPressed && GetValue() + threshold >= 1)
            Pressed();
        
        if(isActive && isPressed && GetValue() - threshold <= 0)
            Released();
    }

    private float GetValue(){
        var value = Vector3.Distance(startPos, transform.localPosition) / joint.linearLimit.limit;

        if (Math.Abs(value) < deadZone)
            value = 0;
        
        return Mathf.Clamp(value, -1f, 1f);
    }
    private void Pressed()
    {
        isPressed = true;
        var cubeRenderer = GetComponentInChildren(typeof(MeshRenderer), true) as MeshRenderer;
            if (cubeRenderer != null)
                cubeRenderer.material.SetColor("_Color", Color.blue);
        onPressed.Invoke();
        Debug.Log("Pressed");
    }

    private void Released()
    {
        isPressed = false;
        onReleased.Invoke();
        Debug.Log("Released");
        var cubeRenderer = GetComponentInChildren(typeof(MeshRenderer), true) as MeshRenderer;
            if (cubeRenderer != null)
                cubeRenderer.material.SetColor("_Color", Color.green);
    }
    
}

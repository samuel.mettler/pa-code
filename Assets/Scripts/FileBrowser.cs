using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;
using System; 

public class FileBrowser : MonoBehaviour
{
    public string path;

    public void OpenFileBrowser()
    {
        if(String.IsNullOrEmpty(path)){
            path = EditorUtility.OpenFilePanel("Select File", "", "");
        }
        RunScript(path);
        
    }

    public void RunScript(string path){

        System.Diagnostics.Process p = new System.Diagnostics.Process();
        p.StartInfo = new System.Diagnostics.ProcessStartInfo(path);
        p.Start(); 
        
    }
}

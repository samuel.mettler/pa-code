using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitPlan : MonoBehaviour
{
    public GameObject pointer;
    private bool isInit;     
    private bool firstIsInit;
    private bool secondIsInit;

    private Vector3 firstClick;
    private Vector3 secondClick;
    private Vector3 thirdClick;

    public Plane currentPlane;
    // Update is called once per frame
    void Start(){

        isInit = false;
    }
    void Update()
    {
        if (!isInit)
        {
            if(Input.GetKeyDown(KeyCode.Space)){
                if(!firstIsInit)
                {
                    firstClick = pointer.transform.position;
                    firstIsInit = true;
                    Debug.Log("First Click");
                    Debug.Log(firstClick);
                    //SwitchPosition(firstClick);
                }
                else if (!secondIsInit){
                    secondClick = pointer.transform.position;   
                    Debug.Log("Second Click");
                    secondIsInit = true;
                    //RotateObject(firstClick, secondClick);
                    //UpdateTransformForScale();
                }
                else {
                    thirdClick =  pointer.transform.position;
                    isInit = true;
                    Debug.Log("Third Click");

                    currentPlane = new Plane(firstClick, secondClick, thirdClick);
                }
            }
        }

        else{
            if (Input.GetKeyDown(KeyCode.N)){
                isInit = false;
                firstIsInit = false;
                Debug.Log("reset");
            }
        }
    }

}
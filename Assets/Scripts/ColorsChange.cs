using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorsChange : MonoBehaviour
{
    // Start is called before the first frame update
    public void ChangesColor(GameObject toColor) 
    {
        var cubeRenderer = toColor.GetComponent<Renderer>();
        cubeRenderer.material.SetColor("_Color", Color.red);
    }
}

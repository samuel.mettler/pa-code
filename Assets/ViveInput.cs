using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ViveInput : MonoBehaviour
{
    public SteamVR_Action_Boolean booleanAction;

    private void Awake() {
        booleanAction = SteamVR_Actions._default.PowerClick;
    }
    // Update is called once per frame
    void Update()
    {
        if(booleanAction[SteamVR_Input_Sources.Any].stateDown)
        {
            Debug.Log("PressedPower buttons 2");
        }
        if(booleanAction.GetStateDown(SteamVR_Input_Sources.Any))
        {
            Debug.Log("PressedPower buttons 3");
        }
        if(SteamVR_Actions._default.PowerClick.GetStateDown(SteamVR_Input_Sources.Any))
        {
            Debug.Log("PressedPower buttons 4");
        }
    }
}
